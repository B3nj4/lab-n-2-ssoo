#ifndef PROCESOS_H
#define PROCESOS_H

//Librerias
#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
#include <string>
using namespace std;
using namespace std;

// Clase procesos
class Procesos {
    private:
        char *url;

    public:
        // Constructor
        Procesos();

        // Metodos
        void descarga(char *url);
        void reproduccion();

};
#endif
