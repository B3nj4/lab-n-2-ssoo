# Lab Nº2 - SSOO 

Dicho laboratorio pretende ejecutar los conocimientos adquiridos en torno a procesos, escencialmente, el uso de los comandos fork() y exec() -y sus variaciones- 
para confrontar diferentes retos. En este caso, se requiere descargar de "Youtube" utilizando la herramienta "youtube-dl" video para ser transformado en formato de audio
-".mp3" en este caso- y luego ser reproducida.

## Requisitos de instalacion

- [ ] Este proyecto requiere la instalacion de las dependencias mediante el comando git clone [url repositorio], luego para utilizarlo debemos cerciorarnos que 
estemos dentro del proyecto y bastaria solo con realizar la linea de comando "make" y luego "./programa [URL]" para su ejecucion. Por ejemplo:

```
git remote clone https://gitlab.com/B3nj4/lab-n-2-ssoo.git
make
./programa https://www.youtube.com/watch?v=dQw4w9WgXcQ

```


## Herramientas

- [ ] Realizado en la IDE Visual Studio Code
- [ ] Uso de paquetes youtube-dl, instalada con la lsiguiente linea de comando:

```
sudo -H pip install --upgrade youtube-dl

```
- [ ] Uso de reproductor "VLC", instalada en el instalador "Ubuntu Sofware" de Ubuntu 
- [ ] Para realizar el proyecto, se utilizao el lenguaje de programacion C++


## Autor
Benjamin Vera Garrido - Ingenieria Civil en Bioinformatica

