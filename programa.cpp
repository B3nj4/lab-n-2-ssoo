#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
#include "procesos.h"
using namespace std;

int main(int argc, char **argv) {
    pid_t pid;
    Procesos proceso;

    // crea el proceso hijo.
    pid = fork();

    // Se evalua si el proceso es inejecutable
    if (pid < 0) {
        cout << "No se pudo crear el proceso ...";
        cout << endl;
        return -1;
      
    } 
    
    // Se evalua si el proceso es ejecutable
    else if (pid == 0) {
        // Código del proceso hijo.
        cout << "\nProceso hijo: ";
        cout << endl;
        
        // Ejecuta comando externo.
        proceso.descarga(argv[1]);
        
    } 
    
    // Continuacion del proceso padre
    else {
        
        // Se espera que termine el proceso hijo.
        wait (NULL);

        // Se ejecuta el proceso padre
        cout << "\nProceso padre: ";
        proceso.reproduccion();

        cout << "Continua con código proceso padre: " << getpid();
        cout << endl;
        
        return 0;
    }

}
